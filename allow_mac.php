<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

// get posted data
$data = json_decode(file_get_contents("php://input"));
$allowed = $data->allowed;
$banned = $data->banned;

$macs = array();
$macs["allowed"] = array();
$macs["banned"] = array();

$names = array();
if ($fh = fopen('map.txt', 'r')) {
    while (!feof($fh)) {
        $line = trim(fgets($fh));
        if (! empty($line)){
            $line = trim($line);
            $words = explode('-',$line);
            $names[$words[1]] = $words[0];
        }
    }
    fclose($fh);
}

if ($fh = fopen('allowed.txt', 'r')) {
    while (!feof($fh)) {
        $line = fgets($fh);
        array_push($macs["allowed"], trim($line));
    }
    fclose($fh);
}

if ($fh = fopen('banned.txt', 'r')) {
    while (!feof($fh)) {
        $line = fgets($fh);
        array_push($macs["banned"], trim($line));
    }
    fclose($fh);
}
//echo $allowed;
$allowed = $names[$allowed];
$banned = $names[$banned];
foreach ($macs["allowed"] as $key => $value) {
    if (trim($value) == $allowed){
        array_push($macs["banned"],$value);
	    unset($macs["allowed"][$key]);
    }
    if ($value == ''){
        unset($macs["allowed"][$key]);
    }
}

foreach ($macs["banned"] as $key => $value) {
    if (trim($value) == $banned){
        array_push($macs["allowed"],$value);
	    unset($macs["banned"][$key]);
    }
    if ($value == ''){
        unset($macs["banned"][$key]);
    }
}

file_put_contents('allowed.txt', implode("\n",$macs["allowed"]));

file_put_contents('banned.txt', implode("\n",$macs["banned"]));

echo json_encode($data);
shell_exec("./sync.sh");
?>
