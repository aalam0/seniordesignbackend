#!/bin/bash

sudo cp allowed.txt /etc/hostapd/accept
sudo cp banned.txt /home/pi/Documents/anomaly_detection/blacklist.txt
sudo cp allowed.txt /home/pi/Documents/anomaly_detection/macs.txt
sudo service hostapd restart
sudo chown -R www-data /var/www/html
