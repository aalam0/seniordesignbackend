<?php
error_reporting(E_ALL);
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

$macs = array();
$macs["allowed"] = array();
$macs["banned"] = array();

$names = array();
if ($fh = fopen('map.txt', 'r')) {
    while (!feof($fh)) {
        $line = trim(fgets($fh));
        if (! empty($line)){
            $line = trim($line);
	    $words = explode('-',$line);
	    $names[$words[0]] = $words[1];
        }
    }
    fclose($fh);
}

//echo json_encode($names);

if ($fh = fopen('allowed.txt', 'r')) {
    while (!feof($fh)) {
        $line = trim(fgets($fh));
	if (! empty($line)){
	    $line = trim($line);
            array_push($macs["allowed"], $names[$line]);
	}
    }
    fclose($fh);
}

if ($fh = fopen('banned.txt', 'r')) {
    while (!feof($fh)) {
        $line = trim(fgets($fh));
        if (! empty($line)){
            $line = trim($line);
            array_push($macs["banned"], $names[$line]);
        }
    }
    fclose($fh);
}

echo json_encode($macs);

?>
